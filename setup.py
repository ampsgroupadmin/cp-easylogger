import os

from setuptools import find_packages, setup

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))


setup(
    name='easylogger',
    version='0.2',
    install_requires=[
        'Django<1.9',
    ],
    include_package_data=True,
    packages=find_packages(),
    license='Not open source',
    description='easylogger fork',
    url='http://www.beefee.co.uk',
    author='BeeFee',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Not open source',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
    ],
)
